# frozen_string_literal: true

require 'rspec'
require './lib/lb/config'

describe Lb::Config do
  subject { described_class.new('gstg') }

  let(:servers) do
    <<~SERVERS
        {
      "results": 9,
      "rows": [
        {
          "fe-altssh-02-lb-gstg": {
            "fqdn": "fe-altssh-02-lb-gstg"
          }
        },
        {
          "fe-01-lb-gstg": {
            "fqdn": "fe-01-lb-gstg"
          }
        }
      ]}
    SERVERS
  end

  before do
    allow(subject).to receive(:knife_result) { servers }
  end

  describe '#lb_list' do
    it 'returns all the LBs' do
      expect(subject.lb_list).to eq('fe-altssh-02-lb-gstg:fe-01-lb-gstg')
    end

    it 'returns web LB' do
      expect(subject.lb_list(role: 'gstg-base-fe-web')).to eq('fe-01-lb-gstg')
    end
  end
  describe '#has_lb?' do
    it 'is true for a LB node' do
      expect(subject.has_lb?('gstg-base-fe-web')).to be_truthy
    end

    it 'is false for a node without LBs' do
      expect(subject.has_lb?('gstg-base-fe-gitaly')).to be_falsey
    end
  end
end
