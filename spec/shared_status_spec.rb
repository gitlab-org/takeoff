# frozen_string_literal: true

require 'spec_helper'
require './lib/shared_status'
require 'timecop'

describe SharedStatus do
  subject(:shared_status) { described_class.new(Time.local(1990)) }

  it 'has a login username' do
    allow(shared_status).to receive(:username).and_return('login_user')

    expect(shared_status.user).to eq('login_user')
  end

  it 'has a total time' do
    Timecop.freeze(Time.local(1990) + 10 * 60) do
      expect(shared_status.total_time).to eq('10 minutes')
    end
  end
end
