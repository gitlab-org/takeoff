# frozen_string_literal: true

require 'spec_helper'
require 'climate_control'

require './lib/steps/smoke_test'

describe Steps::SmokeTest do
  before { enable_dry_run }

  context 'with an invalid environment' do
    subject { described_class.new(Roles.new('fake_env'), environment: 'fake_env') }

    it 'does nothing' do
      expect_no_post

      subject.run
    end
  end

  context 'with a valid environment' do
    subject { described_class.new(Roles.new('gstg'), environment: 'gstg') }

    context 'with no token defined' do
      it 'logs a warning' do
        expect(subject.logger).to receive(:warn)
        expect_no_post

        subject.run
      end
    end

    context 'with a token defined' do
      around do |ex|
        ClimateControl.modify(SMOKE_TEST_STAGING_TRIGGER: 'token') do
          ex.run
        end
      end

      context 'with a failed request' do
        it 'logs a warning' do
          response = double(
            success?: false,
            :[] => { 'message' => 'uhoh' }
          )

          expect_post.and_return(response)
          expect(subject.logger).to receive(:warn)

          subject.run
        end
      end

      context 'with a successful request' do
        it 'triggers the pipeline' do
          response = double(
            success?: true,
            :[] => { 'web_url' => 'https://example.com' }
          )

          expect_post.with(
            "#{described_class::ENDPOINT}/gitlab-org%2Fquality%2Fstaging/trigger/pipeline",
            body: { token: 'token', ref: 'master' }
          ).and_return(response)

          subject.run
        end
      end
    end
  end

  def expect_no_post
    expect(HTTParty).not_to receive(:post)
  end

  def expect_post
    expect(HTTParty).to receive(:post)
  end
end
