# frozen_string_literal: true

require 'spec_helper'
require './lib/version_change'

describe VersionChange do
  describe '#run_command_on_roles' do
    before do
      Takeoff.configure { |config| config[:dry_run] = false }
    end

    after do
      enable_dry_run
    end

    context 'same version' do
      it 'reports the same version' do
        version = described_class.new('1.0.1', '1.0.1', 'fake.file')

        allow(version).to receive(:hosts_for_role)
        allow(version).to receive(:version).and_return('1.0.1', '1.0.1')

        expect(version.changed?).to be false
      end

      it 'shows the IPs with the same version' do
        version = described_class.new('1.0.1', '1.0.1', 'fake.file')
        search_hash =
          {
            "results" => 1,
            "rows" => [
              {
                "deploy.stg.gitlab.com" => {
                  "ipaddress" => "1.2.3.4"
                }
              }
            ]
          }

        allow(JSON).to receive(:parse).and_return(search_hash)

        expect(version.old_version_ips).to eq('1.2.3.4')
      end
    end

    context 'different version' do
      it 'reports that the version differs' do
        version = described_class.new('1.0.1', '1.0.1', 'fake.file')

        allow(version).to receive(:hosts_for_role)
        allow(version).to receive(:version).and_return('1.0.1', '1.0.2')

        expect(version.changed?).to be true
      end
    end
  end
end
