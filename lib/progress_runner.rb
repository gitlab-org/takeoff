require 'open3'
require 'tty-spinner'
require './lib/ansi.rb'
require 'io/wait'
require './lib/time_util'
require './lib/migration/analyzer'
require 'semantic_logger'

class ProgressRunner
  include TimeUtil
  include SemanticLogger::Loggable

  attr_reader :title, :start_time

  def initialize(command, title: nil, silence_stdout: true, spinners: nil, &block)
    @command = command
    @title = title || command
    @silence_stdout = silence_stdout
    @spinners = spinners
    @block = block
  end

  def execute(dry_run: false, verbose: false)
    @dry_run = dry_run
    @verbose = verbose
    @start_time = Time.now

    puts "> #{command}" if verbose?
    logger.debug(title: @title, command: command, stdout: @silence_stdout)

    run_command_with_spinner
  end

  def command
    if dry_run?
      "echo #{@command}"
    else
      @command
    end
  end

  def silence_stdout
    if dry_run?
      false
    else
      @silence_stdout
    end
  end

  private

  def dry_run?
    @dry_run
  end

  def verbose?
    @verbose
  end

  def spinner
    @spinner ||=
      begin
        if @spinners
          sp = @spinners.register(*spinner_options)
          sleep 0.2
          sp
        else
          TTY::Spinner.new(*spinner_options)
        end
      end
  end

  def spinner_options
    [
      ":spinner \e[1m#{title}\e[0m:time_elapsed",
      format: :dots,
      error_mark: ANSI::RED % '✖',
      success_mark: "\e[1m\e[32m✓\e[0m\e[0m"
    ]
  end

  def duration
    Time.now - start_time
  end

  def duration_header
    "(#{time_elapsed(duration, include_ago: false)})"
  end

  def run_command_with_spinner
    Open3.popen3(command) do |_, stdout, stderr, waiter|
      stdout_output = ''
      stderr_output = ''

      loop do
        sleep(0.1) unless dry_run?

        update_spinner

        # Not reading output combined with large enough output can lead to the
        # thread being blocked, thus we read the output in chunks whenever
        # possible.
        write_if_ready(stdout, stdout_output)
        write_if_ready(stderr, stderr_output)

        unless stderr_output.empty?
          logger.error(stderr_output)

          # Chef likes to spit errors on STDOUT too
          logger.debug(stdout_output) unless stdout_output.empty?
        end

        break unless waiter.alive?
      end

      status = waiter.value
      handle_output(status, stdout_output, stderr_output)
    end
  end

  def output_migrations(stdout_output)
    logger.info(stdout_output)

    SemanticLogger.flush
  end

  def handle_output(status, stdout_output, stderr_output)
    if status.success?
      stop_spinner(:success) unless dry_run?

      puts_if_not_empty($stdout, stdout_output) unless silence_stdout
      notify_migrations(stdout_output) if migrations?

      [stdout_output, stderr_output]
    else
      stop_spinner(:error)

      puts_if_not_empty($stdout, stdout_output)
      puts_if_not_empty($stderr, stderr_output)

      raise "Failed to execute command: #{command} - #{stderr_output}"
    end
  end

  def notify_migrations(stdout_output)
    @block.call(stdout_output) if @block
  end

  def update_spinner
    return if dry_run?

    spinner.update(time_elapsed: " (#{time_elapsed(duration)})")
    spinner.spin
  end

  def stop_spinner(status)
    spinner.update(time_elapsed: '')
    spinner.public_send(status, duration_header)
  end

  def write_if_ready(from_io, to)
    block = ''
    block << from_io.read_nonblock(1024) while ready?(from_io)

    return if block.empty?

    output_migrations(block) unless block.empty? || (silence_stdout && !migrations?)

    to << block
  end

  def ready?(from_io)
    from_io.ready? && !from_io.eof?
  end

  def puts_if_not_empty(io, output)
    io.puts(output) unless output.empty?
  end

  def migrations?
    @migrations ||= command =~ /.*db:migrate\'\Z/
  end
end
