# frozen_string_literal: true

require './config/takeoff'
require './config/roles'
require './lib/resumer'

class DeployStatus
  def as_percentage
    return '100% deployment finished.' unless load_state

    "#{percentage}% deployed (running #{step_name})"
  end

  private

  def load_resumer_state
    JSON.parse(File.read(Resumer::STATE_FILE), symbolize_names: true)
  end

  def percentage
    @percentage ||= (current_count * 100 / (step_count + substep_count)).round
  end

  def step_count
    steps.count
  end

  def substep_count
    Roles.new('gprd').regular_no_gitaly.count || 0
  end

  def current_count
    @state[:current_step] + @state[:substeps].size + 1
  end

  def step_name
    @step_name = steps[@state[:current_step]].keys.first
  end

  def steps
    @steps ||= Takeoff.steps
  end

  def load_state
    @state = load_resumer_state if File.exist?(Resumer::STATE_FILE)
  end
end
