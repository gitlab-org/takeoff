module Pid
  def self.create
    File.write(Takeoff.config[:pidfile], Process.pid)
  end

  def self.destroy
    File.delete(Takeoff.config[:pidfile]) if File.exist?(Takeoff.config[:pidfile])
  end
end
